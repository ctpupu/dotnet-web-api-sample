# Dotnet Web API Sample



## Test
Go to solution's root directory.
run `dotnet run`

use Rest Client to test the HTTP Requests
(install VS Code Extension called Rest Client)
```path
.\Requests\Recipe\
```
![plot](./images/rest_client.png)

or

use Postman to test.
![plot](./images/postman.png)