# My Recipe API

##Table of Contents
- [My Recipe API](#my-recipe-api)
    - [Create Recipe](#create-recipe)
        - [Create Recipe Requst](#create-recipe-request)
        - [Create Recipe Response](#create-recipe-response)
    - [Get Recipe](#get-recipe)
        - [Get Recipe Request](#get-recipe-request)
        - [Get Recipe Response](#get-recipe-response)
    - [Update Recipe](#update-recipe)
        - [Put Recipe Request](#put-recipe-request)
        - [Put Recipe Response](#put-recipe-response)
    - [Delete Recipe]
        - [Delete Recipe Request](#delete-recipe-request)
        - [Delete Recipe Response](#delete-recipe-response)

## Create Recipe

### Create Recipe Request
```js
POST /recipes
```

```json
{
    "name": "name",
    "description": "desc",
    "startDateTime": "2024-02-01T00:00:00",
    "endDateTime":"2024-02-01T00:00:00",
    "ingredients":[
        "ingredient 1",
        "ingredient 2"
    ],
    "steps":[
        "step 1",
        "step 2"
    ]
}
```
### Create Recipe Response
`201 Created`
```json
{
    "id": "00000000-0000-0000-0000-000000000000",
    "name": "name",
    "description": "desc",
    "startDateTime": "2024-02-01T00:00:00",
    "endDateTime":"2024-02-01T00:00:00",
    "lastModifiedDateTime":"2024-02-01T00:00:00",
    "ingredients":[
        "ingredient 1",
        "ingredient 2"
    ],
    "steps":[
        "step 1",
        "step 2"
    ]
}
```
`400 Bad Request` on invalid field(s)
```json
{
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "00-de3421df4ca7e70725b33d4cd31c6241-23207cc73e65d8fa-00",
    "errors": {
        "Recipe.InvalidName": [
            "Recipe name must be between 3 and 50 characters long"
        ]
    }
}
```

## Get Recipe

### Get Recipe Request
```js
GET /recipes/{{id}}
```

### Get Recipe Response
`200 OK` on Success
```json
{
    "id": "a8a3cbdb-422c-4e78-9eff-e88ffad9f355",
    "name": "avocado toast",
    "description": "delicious avocado toast",
    "startDateTime": "2024-02-01T08:00:00",
    "endDateTime": "2024-02-01T08:00:00",
    "lastModifiedDateTime": "2024-03-06T02:37:41.7579737Z",
    "ingredients": [
        "cheese",
        "avocado",
        "wheat bread",
        "salt",
        "pepper",
        "chopped tomatoes",
        "minced onion"
    ],
    "steps": [
        "toast the wheat bread with butter on the pan",
        "mashed avocado with salt and petter",
        "add chopped tomatoes and minced onions",
        "mix them well until spreadable",
        "spread avocado spread on the bread",
        "enjoy~!!"
    ]
}
```

`404 Not Found` on recipe not found
```json
{
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
    "title": "Recipe not found",
    "status": 404,
    "traceId": "00-1f3e111ebc65c7766f3f3a126a64c1cb-e69381d4a158596e-00"
}
```

## Update Recipe

### Put Recipe Request
```js
PUT /recipes/{{id}}
```

```json
{
    "name": "name",
    "description": "desc",
    "startDateTime": "2024-02-01T00:00:00",
    "endDateTime":"2024-02-01T00:00:00",
    "ingredients":[
        "ingredient 1",
        "ingredient 2"
    ],
    "steps":[
        "step 1",
        "step 2"
    ]
}
```

### Put Recipe Response
`204 No Content` on Success

`201 Created` on create new recipe
```json
{
    "id": "00000000-0000-0000-0000-000000000000",
    "name": "name",
    "description": "desc",
    "startDateTime": "2024-02-01T00:00:00",
    "endDateTime":"2024-02-01T00:00:00",
    "lastModifiedDateTime":"2024-02-01T00:00:00",
    "ingredients":[
        "ingredient 1",
        "ingredient 2"
    ],
    "steps":[
        "step 1",
        "step 2"
    ]
}
```

## Delete Recipe

### Delete Recipe Request
```js
DELETE /recipes/{{id}}
```

### Delete Recipe Response
`204 No Content` on Success