using ErrorOr;
using MyRecipe.ServiceErrors;

namespace MyRecipe.Models
{
    public class Recipe
    {
        public const int MinNameLength = 3;
        public const int MaxNameLength = 50;
        public const int MinDescriptionLength = 1;
        public const int MaxDescriptionLength = 150;

        
        public Guid Id { get; }
        public string Name { get; }
        public string Description { get; }
        public DateTime StartDateTime { get; }
        public DateTime EndDateTime { get; }
        public DateTime LastModifiedDateTime { get; }
        public List<string> Ingredients { get; }
        public List<string> Steps { get; }

        private Recipe(Guid id, string name, string description, DateTime startDate, DateTime endDate, DateTime lastModified, List<string> ingredients, List<string> steps)
        {
            Id = id;
            Name = name;
            Description = description;
            StartDateTime = startDate;
            EndDateTime = endDate;
            LastModifiedDateTime = lastModified;
            Ingredients = ingredients;
            Steps = steps;
        }

        public static ErrorOr<Recipe> Create(
            string name,
            string description,
            DateTime startDateTime,
            DateTime endDateTime,
            List<string> ingredients,
            List<string> steps,
            Guid? id = null)
        {
            List<Error> errors = new List<Error>();
            if(name.Length < MinNameLength || 
                name.Length > MaxNameLength) 
            {
                errors.Add(Errors.Recipe.InvalidName);
            }

            if(description.Length < MinDescriptionLength ||
                description.Length > MaxDescriptionLength)
            {
                errors.Add(Errors.Recipe.InvalidDescription);   
            }

            if(errors.Count > 0) 
            {
                return errors;
            }

            return new Recipe(
                id == null ? Guid.NewGuid() : id.GetValueOrDefault(),
                name,
                description,
                startDateTime,
                endDateTime,
                DateTime.UtcNow,
                ingredients,
                steps);
        }
    }

}