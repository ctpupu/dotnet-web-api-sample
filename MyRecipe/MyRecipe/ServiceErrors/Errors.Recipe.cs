using ErrorOr;

namespace MyRecipe.ServiceErrors
{
    public static class Errors
    {
        public static class Recipe
        {
            public static Error InvalidName => Error.Validation(
                code: "Recipe.InvalidName",
                description: $"Recipe name must be between {Models.Recipe.MinNameLength} and {Models.Recipe.MaxNameLength} characters long");

            public static Error InvalidDescription => Error.Validation(
                code: "Recipe.InvalidDescription",
                description: $"Recipe description must be between {Models.Recipe.MinDescriptionLength} and {Models.Recipe.MaxDescriptionLength} characters long");

            public static Error NotFound => Error.NotFound(
                code: "Recipe.NotFound",
                description: "Recipe not found");
        }
    }
}