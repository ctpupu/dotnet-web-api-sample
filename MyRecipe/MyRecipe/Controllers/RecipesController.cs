using ErrorOr;
using Microsoft.AspNetCore.Mvc;
using MyRecipe.Contracts.Recipe;
using MyRecipe.Models;
using MyRecipe.Services.Recipes;

namespace MyRecipe.Controllers;

public class RecipesController: ApiController
{
    private readonly IRecipeService _recipeService;

    public RecipesController(IRecipeService recipeService) 
    {
        _recipeService = recipeService;
    }

    [HttpPost]
    public IActionResult CreateRecipe([FromBody] CreateRecipeRequest request)
    {
        ErrorOr<Recipe> requestToRecipeResult = Recipe.Create(
            request.Name,
            request.Description,
            request.StartDateTime,
            request.EndDateTime,
            request.Ingredients,
            request.Steps);

        if(requestToRecipeResult.IsError) 
        {
            return Problem(requestToRecipeResult.Errors);
        }

        var recipe = requestToRecipeResult.Value;
        var createRecipeResult = _recipeService.CreateRecipe(recipe);

        return createRecipeResult.Match(
            created => CreatedAtGetRecipe(recipe),
            errors => Problem(errors));
    }

    [HttpGet("{id:guid}")]
    public IActionResult GetRecipe(Guid id) 
    {
        ErrorOr<Recipe> getRecipeResult = _recipeService.GetRecipe(id);

        return getRecipeResult.Match(
            recipe => Ok(MapRecipeResponse(recipe)),
            errors => Problem(errors));
    }


    [HttpPut("{id:guid}")]
    public IActionResult UpsertRecipe(Guid id, UpsertRecipeRequest request) 
    {
        ErrorOr<Recipe> requestToRecipeResult = Recipe.Create(
            request.Name,
            request.Description,
            request.StartDateTime,
            request.EndDateTime,
            request.Ingredients,
            request.Steps,
            id);

        if(requestToRecipeResult.IsError) 
        {
            return Problem(requestToRecipeResult.Errors);
        }

        var recipe = requestToRecipeResult.Value;
        ErrorOr<UpsertedRecipe> upsertRecipeResult = _recipeService.UpsertRecipe(recipe);

        return upsertRecipeResult.Match(
            upserted => upserted.IsNewlyCreated ? CreatedAtGetRecipe(recipe) : NoContent(),
            errors => Problem(errors));
    }

    [HttpDelete("{id:guid}")]
    public IActionResult DeleteRecipe(Guid id) 
    {
        var deleteRecipeResult = _recipeService.DeleteRecipe(id);
        return deleteRecipeResult.Match(
            deleted => NoContent(),
            errors => Problem(errors));
    }
    private static RecipeResponse MapRecipeResponse(Recipe recipe) {
        return new RecipeResponse(
                recipe.Id,
                recipe.Name,
                recipe.Description,
                recipe.StartDateTime,
                recipe.EndDateTime,
                recipe.LastModifiedDateTime,
                recipe.Ingredients,
                recipe.Steps);
    }

    private CreatedAtActionResult CreatedAtGetRecipe(Recipe recipe)
    {
        return CreatedAtAction(
            actionName: nameof(GetRecipe),
            routeValues: new { id = recipe.Id },
            value: MapRecipeResponse(recipe));
    }
}