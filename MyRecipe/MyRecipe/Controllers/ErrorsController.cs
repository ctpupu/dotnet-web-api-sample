using Microsoft.AspNetCore.Mvc;

namespace MyRecipe.Controllers
{
    public class ErrorsController : ControllerBase
    {
        [Route("/error")]
        public IActionResult Error()
        {   
            return Problem();
        }
    }
}