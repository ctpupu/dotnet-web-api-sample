namespace MyRecipe.Services.Recipes
{
    public record struct UpsertedRecipe(bool IsNewlyCreated);
}