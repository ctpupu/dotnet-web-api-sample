
namespace MyRecipe.Contracts.Recipe 
{
    public record struct UpsertRecipeRequest(
        string Name,
        string Description,
        DateTime StartDateTime,
        DateTime EndDateTime,
        List<string> Ingredients,
        List<string> Steps
    );
}