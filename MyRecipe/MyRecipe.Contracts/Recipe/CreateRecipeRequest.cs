
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace MyRecipe.Contracts.Recipe 
{
    public record struct CreateRecipeRequest(
        string Name,
        string Description,
        DateTime StartDateTime,
        DateTime EndDateTime,
        List<string> Ingredients,
        List<string> Steps
    );
}